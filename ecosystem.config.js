function getAppConfig(service) {
    return {
        name: service,
        script: 'dist/index.js',
        cwd: `./service-${service}/`,
        // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
        args: '',
        instances: 1,
        autorestart: true,
        watch: false,
        max_memory_restart: '1G',
    }
}

module.exports = {
    apps: [
        getAppConfig('gateway'),
        getAppConfig('users'),
        getAppConfig('article'),
        getAppConfig('files'),
        getAppConfig('simbad'),
        getAppConfig('telegram-bot')
    ]
};
