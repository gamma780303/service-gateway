import {config} from "../env/env";
import {App} from "package-app";
import {AuthPayload, AuthResult, ServiceName, UsersActionName} from "package-types";

export async function authenticate (req: any, res: any, next: any) {
    const access: string = req.headers['authorization'];
    req.$params.currentUser = undefined;
    if(access) {
        if(!config.accessTokenSecret) {
            throw new Error('No secret word for authentication provided!');
        }
        try {
            const result = await App.call<AuthPayload, AuthResult>(ServiceName.Users, UsersActionName.Auth, { access });
            if(!result.currentUser) {
                throw new Error('Unauthorized');
            }
            req.$params.currentUser = result.currentUser;
        } catch (err) {
            App.logError(err);
            res.statusCode = 401;
        }
    }
    next();
}
