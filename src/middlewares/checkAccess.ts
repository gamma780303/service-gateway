import {Role} from "package-types";

export function isAdmin(req: any, res: any, next: any) {
    const { currentUser } = req.$params;
    if(!currentUser) {
        res.statusCode = 403;
        res.end();
        return;
    }
    const { role } = currentUser;
    if(role !== Role.ADMIN) {
        res.statusCode = 403;
        res.end();
        return;
    }
    next();
}

export function isModerator(req: any, res: any, next: any) {
    const { currentUser } = req.$params;
    if(!currentUser) {
        res.statusCode = 403;
        res.end();
        return ;
    }
    const { role } = currentUser;
    if(role !== Role.MODERATOR) {
        res.statusCode = 403;
        res.end();
        return;
    }
    next();
}

export function isAuthor(req: any, res: any, next: any) {
    const { currentUser } = req.$params;
    if(!currentUser) {
        res.statusCode = 403;
        res.end();
        return;
    }
    const { role } = currentUser;
    if(role !== Role.AUTHOR) {
        res.statusCode = 403;
        res.end();
        return;
    }
    next();
}
