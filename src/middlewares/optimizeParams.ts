export default function optimizeParams(req: any, res: any, next: any) {
    const {$params} = req;
    if(!$params) {
        return next();
    }
    Object.keys($params).forEach((key) => {
        if(key.includes('[]')) {
            const optimizedKey = key.replace('[]', '');
            if (!Array.isArray($params[key])) {
                req.$params[optimizedKey] = [$params[key]];
            } else {
                req.$params[optimizedKey] = $params[key];
            }
        }
    });

    next();
}
