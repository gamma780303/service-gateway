export default function fileUpload(req: any, res: any, next: any) {
    const {files} = req;
    if(!files) {
        return next();
    }
    Object.keys(files).forEach((key) => {
        const optimizedKey = key.replace('[]', '');
        req.$params[optimizedKey] = files[key];
    });

    next();
}
