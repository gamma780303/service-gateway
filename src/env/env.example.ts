import { ServiceConfig } from 'package-app';

export const config: ServiceConfig = {
    name: 'gateway',
    brokerConfig: {
        nodeID: 'gateway',
        transporter: 'Redis',
        logLevel: 'debug',
        logger: true
    },
    accessTokenSecret: 'authAccessSecret123123',
    refreshTokenSecret: 'authRefreshSecret123123'
}
