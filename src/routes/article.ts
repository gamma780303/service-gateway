import {HttpMethod, Route} from "package-app";
import {ArticleActionName, Role, ServiceName} from "package-types";
import fileUpload from "../middlewares/fileUpload";

export const routes: Route[] = [
    {
        path: '/article/proposeArticle',
        method: HttpMethod.POST,
        role: Role.USER,
        actionName: ArticleActionName.RequestAddArticle,
        serviceName: ServiceName.Article,
        middlewares: [fileUpload]
    },
    {
        path: '/article/getArticlesForReview',
        method: HttpMethod.GET,
        role: Role.AUTHOR,
        actionName: ArticleActionName.GetArticlesForReview,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: '/article/approveArticle',
        method: HttpMethod.POST,
        role: Role.AUTHOR,
        actionName: ArticleActionName.ApproveArticle,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: '/article/editArticle/:articleId',
        method: HttpMethod.PUT,
        role: Role.USER,
        actionName: ArticleActionName.EditArticle,
        serviceName: ServiceName.Article,
        middlewares: [fileUpload]
    },
    {
        path: '/article/rejectArticle',
        method: HttpMethod.POST,
        role: Role.AUTHOR,
        actionName: ArticleActionName.RejectArticle,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: '/article/getRemarks/:articleId',
        method: HttpMethod.GET,
        role: Role.USER,
        actionName: ArticleActionName.GetRemarks,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: '/article/getArticlesByAuthor',
        method: HttpMethod.GET,
        role: Role.GUEST,
        actionName: ArticleActionName.GetArticlesByAuthor,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: '/article/getArticle/:id',
        method: HttpMethod.GET,
        role: Role.GUEST,
        actionName: ArticleActionName.GetArticle,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: '/article/delete/:articleId',
        method: HttpMethod.DELETE,
        role: Role.AUTHOR,
        actionName: ArticleActionName.DeleteArticle,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: '/article/comment',
        method: HttpMethod.POST,
        role: Role.USER,
        actionName: ArticleActionName.AddComment,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: 'article/deleteComment/:commentId',
        method: HttpMethod.DELETE,
        role: Role.MODERATOR,
        actionName: ArticleActionName.DeleteComment,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: 'article/reportComment/:commentId',
        method: HttpMethod.POST,
        role: Role.USER,
        actionName: ArticleActionName.AddReport,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: 'article/getReports',
        method: HttpMethod.GET,
        role: Role.MODERATOR,
        actionName: ArticleActionName.GetReports,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: 'article/deleteReport/:reportId',
        method: HttpMethod.DELETE,
        role: Role.MODERATOR,
        actionName: ArticleActionName.DeleteReport,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: 'article/getComments/:articleId',
        method: HttpMethod.GET,
        role: Role.GUEST,
        actionName: ArticleActionName.GetComments,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: '/article/searchArticles',
        method: HttpMethod.GET,
        role: Role.GUEST,
        actionName: ArticleActionName.GetArticles,
        serviceName: ServiceName.Article,
        middlewares: []
    },
    {
        path: '/article/getCommentsByAuthor',
        method: HttpMethod.GET,
        role: Role.GUEST,
        actionName: ArticleActionName.GetCommentsByAuthor,
        serviceName: ServiceName.Article,
        middlewares: []
    },
];
