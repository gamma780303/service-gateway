import {HttpMethod, Route} from "package-app";
import {Role, ServiceName, UsersActionName} from "package-types";
import fileUpload from '../middlewares/fileUpload';
import {GatewayActionName} from "package-types/dist/actions/gateway";

export const routes: Route[] = [
    {
        path: '/users/login',
        method: HttpMethod.POST,
        role: Role.GUEST,
        actionName: UsersActionName.Login,
        serviceName: ServiceName.Users,
        middlewares: []
    },
    {
        path: '/users/register',
        method: HttpMethod.POST,
        role: Role.GUEST,
        actionName: UsersActionName.Register,
        serviceName: ServiceName.Users,
        middlewares: [fileUpload]
    },
    {
        path: '/users/refresh',
        method: HttpMethod.POST,
        role: Role.GUEST,
        actionName: UsersActionName.Refresh,
        serviceName: ServiceName.Users,
        middlewares: []
    },
    {
        path: '/users/auth',
        method: HttpMethod.GET,
        role: Role.GUEST,
        actionName: GatewayActionName.None,
        serviceName: ServiceName.Gateway,
        middlewares: []
    },
    {
        path: '/users/:userId',
        method: HttpMethod.GET,
        role: Role.USER,
        actionName: UsersActionName.GetUser,
        serviceName: ServiceName.Users,
        middlewares: []
    },
    {
        path: '/users/changeAvatar',
        method: HttpMethod.PUT,
        role: Role.USER,
        actionName: UsersActionName.ChangeAvatar,
        serviceName: ServiceName.Users,
        middlewares: [fileUpload]
    },
    {
        path: '/users/changeNickname',
        method: HttpMethod.PUT,
        role: Role.USER,
        actionName: UsersActionName.ChangeNickname,
        serviceName: ServiceName.Users,
        middlewares: []
    },    {
        path: '/users/changePassword',
        method: HttpMethod.PUT,
        role: Role.USER,
        actionName: UsersActionName.ChangePassword,
        serviceName: ServiceName.Users,
        middlewares: []
    },    {
        path: '/users/resetPassword',
        method: HttpMethod.PUT,
        role: Role.USER,
        actionName: UsersActionName.ResetPassword,
        serviceName: ServiceName.Users,
        middlewares: []
    },
    {
        path: '/users/clearTokens',
        method: HttpMethod.DELETE,
        role: Role.USER,
        actionName: UsersActionName.ClearTokens,
        serviceName: ServiceName.Users,
        middlewares: []
    },
    {
        path: '/users/deleteUser/:userId',
        method: HttpMethod.DELETE,
        role: Role.USER,
        actionName: UsersActionName.DeleteUser,
        serviceName: ServiceName.Users,
        middlewares: []
    },
    {
        path: '/users/getUsersByRole',
        method: HttpMethod.GET,
        role: Role.ADMIN,
        actionName: UsersActionName.GetUsersByRole,
        serviceName: ServiceName.Users,
        middlewares: []
    },
    {
        path: '/users/changeRole',
        method: HttpMethod.PUT,
        role: Role.ADMIN,
        actionName: UsersActionName.ChangeRole,
        serviceName: ServiceName.Users,
        middlewares: []
    }
]
