import {Aliases} from "package-app";
import { routes as ArticleRoutes } from './article';
import { routes as AdminRoutes } from './admin';
import { routes as SimbadRoutes } from './simbad';
import { routes as UsersRoutes } from './users';
import {authenticate} from "../middlewares/auth";
import {Role, UsersActionName} from "package-types";
import {isAuthor, isModerator, isAdmin} from "../middlewares/checkAccess";
import optimizeParams from "../middlewares/optimizeParams";
import {GatewayActionName} from "package-types/dist/actions/gateway";
export default function getRoutes(): Aliases {
    const routes = [...ArticleRoutes, ...AdminRoutes, ...SimbadRoutes, ...UsersRoutes];
    const aliases: Aliases = {};
    routes.forEach((route) => {
        const { path, method, role, actionName, serviceName, middlewares } = route;
        const routeMiddlewares = <Array<Function | string>>middlewares
        routeMiddlewares.push(optimizeParams);
        if(actionName !== UsersActionName.Login && actionName !== UsersActionName.Register && actionName !== UsersActionName.Refresh) {
            routeMiddlewares.push(authenticate);
        }
        if(role) {
            switch (role) {
                case Role.ADMIN:
                    routeMiddlewares.push(isAdmin);
                    break;
                case Role.MODERATOR:
                    routeMiddlewares.push(isModerator);
                    break;
                case Role.AUTHOR:
                    routeMiddlewares.push(isAuthor);
                    break;
                default:
                    break;
            }
        }
        if(actionName !== GatewayActionName.None) {
            routeMiddlewares.push(`${serviceName}.${actionName}`);
        } else {
            routeMiddlewares.push(() => {
                return {};
            });
        }
        aliases[`${method} ${path}`] = middlewares
    });
    return aliases;
}
