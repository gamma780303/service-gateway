import {App} from 'package-app';
import {config} from './env/env';
import ApiService from 'moleculer-web'
import getRoutes from "./routes/routesHelper";
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import expressFileUpload from 'express-fileupload';

App.getInstance().run({
    mixins: [ApiService],

    settings: {
        port: 3080,
        use: [
            cors({
                origin: ['http://localhost:3000', 'http://127.0.0.1:3000'],
                credentials: true,
            }),
            bodyParser({limit: '100mb'}),
            cookieParser(),
            expressFileUpload()
        ],
        routes: [{
            aliases: getRoutes()
        }]
    },
    logger: true,
    logLevel: 'info',
    name: config.name,
}).then();
